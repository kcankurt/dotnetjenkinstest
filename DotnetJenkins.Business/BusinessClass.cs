﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotnetJenkins.Business
{
    public class BusinessClass
    {
        public string HelloWorld()
        {
            return "hello world!";
        }

        public void UnCoveredMethod()
        {
            //foo bar
        }

        public void UnCoveredMethodString(string s)
        {
            //foo bar
        }

        public void UnCoveredMethodInt(int i)
        {
            //foo bar
        }
    }
}
