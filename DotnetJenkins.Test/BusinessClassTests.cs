﻿using NUnit.Framework;

namespace DotnetJenkins.Business.Tests
{
    [TestFixture()]
    public class BusinessClassTests
    {
        [Test()]
        public void HelloWorldTest()
        {
            //Arrange
            var businessClass = new BusinessClass();

            //Act
            var sut = businessClass.HelloWorld();

            //Assert
            Assert.AreEqual("hello world!",sut);
        }
    }
}